#include <stdio.h>
#include <string.h>

int main(){
    char string1[] = "pisica";
    char string2[] = "pisica";
    int rezultat = strcmp(string1, string2); 
    if (rezultat < 0){
        printf("%s %s\n", string1, string2);
    } else if(rezultat > 0){
        printf("%s %s\n", string2, string1);
    } else {
        printf("%s == %s\n", string1, string2);
    }
    return 0;
}
