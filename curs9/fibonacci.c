#include <stdio.h>

int fibonacci (int n) {
    int term;
    if (n == 0 || n == 1) {
        term = 1;
    } else {
        term = fibonacci (n - 1) + fibonacci (n - 2);
    }
    
    return term;
}

int main(){
    int n;
    printf("n = ");
    scanf("%d", &n);
    int i;
    for (i = 0; i < n; i++) {
        printf("%d ", fibonacci(i));
    }
    printf("\n");
    
    return 0;
}

