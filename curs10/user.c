#include <user.h>
#include <stdio.h>
#include <string.h>

int main(){
    struct User user;
    printf("Give name: ");
    fgets(user.name, 100, stdin);
    user.name[strlen(user.name) - 1] = 0;
   
    printf("Give address: ");
    fgets(user.address, 255, stdin);
    user.address[strlen(user.address) - 1] = 0;

    printf("Give CNP: ");
    fgets(user.cnp, 14, stdin);
    user.cnp[strlen(user.cnp) - 1] = 0;

    int age;
    printf("Give age: ");
    scanf("%d", &age);
    user.age = age;

    printUser(user);
    
    return 0;
}
